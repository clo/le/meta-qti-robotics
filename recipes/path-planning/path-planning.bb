DESCRIPTION = "path-planning module"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"
LICENSE = "BSD"
inherit cmake

PR = "r0"
PV = "1.0"


FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/robotics-oss:"
S = "${WORKDIR}/navigation-stack/path-planning/"
#SRC_DIR = "${WORKSPACE}/vendor/qcom/opensource/robotics-oss/navigation-stack/path-planning/"
SRC_URI = "file://navigation-stack/path-planning"

FILES_${PN} += "/data/pathplan/*"

PACKAGES = "${PN}"

do_install() {
    dest=/data/pathplan/
    install -d ${D}${dest}
#    install -m 755 ${WORKDIR}/navigation-stack/path-planning/* -D ${D}${dest}
    cp -rf ${S}/* ${D}${dest}
}
