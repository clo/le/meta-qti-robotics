inherit autotools pkgconfig

DESCRIPTION = "Camera Framework"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=89aea4e17d99a7cacdbeed46a0096b10"

PR = "r1"

DEPENDS += "binder"
DEPENDS += "liblog"
DEPENDS += "libcutils"
DEPENDS += "libhardware"
DEPENDS += "libui"
DEPENDS += "adreno"
DEPENDS += "system-core"
DEPENDS += "libjpeg-turbo"
DEPENDS += "media-headers"
DEPENDS += "camera-metadata"

FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/robotics-oss/:"
SRC_URI = "file://camera-frameworks/camera-server"
SRC_URI += "file://camera-server.service"

S = "${WORKDIR}/camera-frameworks/camera-server"

EXTRA_OECONF += " --with-core-includes=${WORKSPACE}/system/core/include --with-glib"
EXTRA_OECONF += " --with-kernel-headers=${STAGING_KERNEL_BUILDDIR}/include/uapi"

FILES_${PN}-dbg    = "${libdir}/.debug/libcamera1_client.* ${bindir}/.debug/*"
FILES_${PN}        = "${libdir}/libcamera1_client.so.* ${libdir}/pkgconfig/* ${libdir}/libcameraservice.so.* ${bindir}/cameraserver"
FILES_${PN}-dev    = "${libdir}/libcamera1_client.so ${libdir}/libcamera1_client.la ${includedir}"

LDFLAGS += "-llog -lhardware -lutils -lcutils"

CPPFLAGS += "-I${STAGING_INCDIR}/camera"
CPPFLAGS += "-I${STAGING_KERNEL_BUILDDIR}/usr/include"
CPPFLAGS += "-I${WORKSPACE}/frameworks/native/include/media/openmax"
CPPFLAGS += "-I${WORKSPACE}/hardware/qcom/media/mm-core/inc"
CPPFLAGS += "-I${WORKSPACE}/hardware/libhardware/include"
CPPFLAGS += "-I${WORKSPACE}/system/core/include"
CPPFLAGS += "-fpermissive"

# When kernel is built using sstate, the STAGING_KERNEL_DIR is not populated because do_shared_workdir is non-sstatable. Hence, to ensure the task executes, add an explicit dependency on it.
do_configure[depends] += "virtual/kernel:do_shared_workdir"

do_install_append() {
   if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
       install -d ${D}${systemd_unitdir}/system/
       install -m 0644 ${WORKDIR}/camera-server.service -D ${D}${systemd_unitdir}/system/camera-server.service
       install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
       # enable the service for multi-user.target
       ln -sf ${systemd_unitdir}/system/camera-server.service \
              ${D}${systemd_unitdir}/system/multi-user.target.wants/camera-server.service
   fi
}

FILES_${PN} += "${systemd_unitdir}/system/"
