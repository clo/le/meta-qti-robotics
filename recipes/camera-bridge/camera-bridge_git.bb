inherit cmake pkgconfig

DESCRIPTION = "camera bridge for vSLAM"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/robotics-oss:"
SRC_URI   =  "file://camera-frameworks/camera-bridge/"
SRC_URI   =+ "file://camera-frameworks/camera-server/include/"
SRC_DIR   =  "${WORKSPACE}/vendor/qcom/opensource/robotics-oss/camera-frameworks/camera-bridge/"
S         =  "${WORKDIR}/camera-frameworks/camera-bridge/"

DEPENDS   =  "liblog"
DEPENDS   += "libcutils"
DEPENDS   += "system-core"
DEPENDS   += "glib-2.0"
DEPENDS   += "virtual/kernel"
DEPENDS   += "camera-server"
DEPENDS   += "chicdk"
DEPENDS   += "binder"

EXTRA_OECMAKE = "\
    -DCMAKE_CROSSCOMPILING:BOOL=True \
    -DKERNEL_INCDIR=${STAGING_KERNEL_BUILDDIR} \
    -DCMAKE_LIBRARY_PATH:PATH=${STAGING_LIBDIR} \
    -DCMAKE_INCLUDE_PATH:PATH=${STAGING_INCDIR} \
    -DCMAKE_SYSROOT:PATH=${STAGING_DIR_HOST} \
    -DWORKSPACE=${WORKSPACE} \
"

FILES_${PN} = "\
    /usr/lib/* \
    /usr/bin/*"

FILES_${PN}-dev = ""
#Skips check for .so symlinks
INSANE_SKIP_${PN} = "dev-so"
