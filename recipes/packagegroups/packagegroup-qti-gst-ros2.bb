SUMMARY = "QTI Gst Ros2 Package Group"

LICENSE = "BSD"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PROVIDES = "${PACKAGES}"

PACKAGES = ' \
    packagegroup-qti-gst-ros2 \
'

RDEPENDS_packagegroup-qti-gst-ros2 = ' \
    ${@bb.utils.contains("COMBINED_FEATURES", "qti-gst-ros2", "gst-ros2sink gst-ros2node", "", d)} \
'

