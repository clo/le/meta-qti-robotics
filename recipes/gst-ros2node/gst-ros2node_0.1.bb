inherit cmake pkgconfig python3native

SUMMARY  = "QTI open-source ROS2 node based on Gstreamer"
iSECTION = "multimedia"
LICENSE  = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/${LICENSE};md5=3775480a712fc46a69647678acb234cb"

TARGET_SYS               = "aarch64-linux-gnu"
APPEND_FLAGS             = "-target ${TARGET_SYS} "
APPEND_FLAGS            += "-I ${STAGING_INCDIR}/c++ "
OECMAKE_C_FLAGS_append   = "${APPEND_FLAGS}"
OECMAKE_CXX_FLAGS_append = "${APPEND_FLAGS}"

# Dependencies.
DEPENDS += "virtual/kernel"
DEPENDS += "system-core-headers"
DEPENDS += "llvm-arm-toolchain-native"
DEPENDS += "glib-2.0"
DEPENDS += "gstreamer1.0"
DEPENDS += "python3-pyparsing"
DEPENDS += "ros2-dashing"
DEPENDS += "liblog"

FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/robotics-oss:"
SRC_URI   =  "file://gst-ros2node/"
S         =  "${WORKDIR}/gst-ros2node/"

DASHING_DIR    := "/opt/ros/dashing"
INSTALL_LIBDIR := "/opt/ros/dashing/lib"
EXTRA_OECMAKE  += "-DKERNEL_INCDIR=${STAGING_KERNEL_BUILDDIR}"
EXTRA_OECMAKE  += "-DGST_VERSION_REQUIRED=1.14.4"
EXTRA_OECMAKE  += "-DSYSROOT_INCDIR=${STAGING_INCDIR}"
EXTRA_OECMAKE  += "-DSYSROOT_LIBDIR=${STAGING_LIBDIR}"
EXTRA_OECMAKE  += "-DDASHING_DIR=${RECIPE_SYSROOT}/${DASHING_DIR}"
EXTRA_OECMAKE  += "-DINSTALL_LIBDIR=${INSTALL_LIBDIR}"
EXTRA_OECMAKE  += "-DPLATFORM:STRING=linux"
EXTRA_OECMAKE  += "-DCPU:STRING=64"
EXTRA_OECMAKE  += "-DENABLE_LIBLOG:BOOL=True"
EXTRA_OECMAKE  += "-DCMAKE_CROSSCOMPILING:BOOL=True"
EXTRA_OECMAKE  += "-DCMAKE_C_COMPILER:STRING=${OECMAKE_C_COMPILER}"

export PYTHONPATH = "${RECIPE_SYSROOT}${PYTHON_SITEPACKAGES_DIR}"

do_configure[depends] += "virtual/kernel:do_shared_workdir"
do_configure_prepend() {
    echo "PYTHONPATH=${PYTHONPATH}"
}

do_install_append() {
    install -d ${D}/data/misc/ros2/
    install -d ${D}/${INSTALL_LIBDIR}/

    if [ -d "${WORKDIR}/gst-ros2node/config/" ];then
        cp -rf ${WORKDIR}/gst-ros2node/config/*.xml ${D}/data/misc/ros2/
    fi
    if [ -d "${D}/usr/share/" ];then
        cp -rf ${D}/usr/share/ ${D}/${DASHING_DIR}/
        rm -rf ${D}/usr
    fi
}

FILES_${PN} += "\
    ${DASHING_DIR}/* \
    /data/* "

SOLIBS = ".so*"
FILES_SOLIBSDEV = ""
do_package_qa[noexec] = "1"
#RM_WORK_EXCLUDE += "${PN}"