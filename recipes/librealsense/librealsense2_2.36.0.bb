SUMMARY = "Library for Intel Realsense D435i"
HOMEPAGE = "https://github.com/IntelRealSense/librealsense"
SECTION = "libs"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit cmake pkgconfig
SRC_URI = "git://github.com/IntelRealSense/librealsense.git;protocol=https;tag=v${PV}"

S = "${WORKDIR}/git"

EXTRA_OECMAKE += " \
    -DBUILD_SHARED_LIBS=ON \
    -DBUILD_WITH_TM2=OFF \
    -DBUILD_EXAMPLES=ON \
    -DFORCE_RSUSB_BACKEND=ON \
    -DBUILD_GRAPHICAL_EXAMPLES=OFF \
    -DBUILD_GLSL_EXTENSIONS=OFF "

OECMAKE_FIND_ROOT_PATH_MODE_PROGRAM_class-target_qrb5165-rb5 = "BOTH"

DEPENDS = "udev libusb1 ccache-native"

RDEPENDS_${PN}-tests += "${PN}"

PACKAGES =+ "${PN}-tests"

FILES_${PN} = "${libdir}/${PN}.so.* \
       ${sysconfdir}/udev/rules.d/* "

FILES_${PN}-tests = "${bindir}/rs-color \
       ${bindir}/rs-depth \
       ${bindir}/rs-distance \
       ${bindir}/rs-save-to-disk \
       ${bindir}/rs-enumerate-devices \
       ${bindir}/rs-pose \
       ${bindir}/rs-fw-logger "

do_install_append() {
    install -d "${D}${sysconfdir}/udev/rules.d"
    install -m 0644 ${S}/config/99-realsense-libusb.rules ${D}${sysconfdir}/udev/rules.d/99-${PN}-libusb.rules
}
